package com.fengye.demo;

import com.fengye.demo.configuration.SpringContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.TransactionManager;

@SpringBootTest
@Slf4j
class AopEventDemoApplicationTests {

	@Test
	void contextLoads() {
		TransactionManager transactionManager = SpringContextUtil.getBean(TransactionManager.class);
		log.info(String.valueOf(transactionManager));
	}

}
