package com.fengye.demo.dto;

import lombok.Data;

/**
 * @author fengyexjtu@126.com
 * @since 2022-10-05
 */
@Data
public class ResultDTO {
    private String processInstanceId;
}
