package com.fengye.demo.service.impl;

import com.fengye.demo.annotation.Workflow;
import com.fengye.demo.enums.OperationType;
import com.fengye.demo.service.IWorkflowTransactionEventService;
import com.fengye.demo.vo.WorkflowVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author fengyexjtu@126.com
 * @since 2022-10-05
 */
@Service
@Slf4j
public class WorkflowTransactionEventService implements IWorkflowTransactionEventService {
    @Workflow(operation = OperationType.START, isRecordLog = true)
    @Override
    public WorkflowVO startProcess(WorkflowVO workflowVO) {
        log.info("WorkflowTransactionEventService workflowVO ===> {}", workflowVO);
        return workflowVO;
    }
}
