package com.fengye.demo.service.impl;

import com.fengye.demo.mapper.CustomerMapper;
import com.fengye.demo.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ICustomerServiceImpl implements ICustomerService {

    @Autowired
    private CustomerMapper customerMapper;
    @Override
    public int save() {
        return customerMapper.insert(buildCustomer());
    }
}
