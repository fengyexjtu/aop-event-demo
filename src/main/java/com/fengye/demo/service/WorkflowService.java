package com.fengye.demo.service;

import com.fengye.demo.dto.ResultDTO;
import com.fengye.demo.vo.ResultVO;
import com.fengye.demo.vo.WorkflowVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author fengyexjtu@126.com
 * @since 2022-10-05
 */
@Service
@Slf4j
public class WorkflowService {
    public ResultVO startFlow(WorkflowVO workflowVO) {
        log.info("WorkflowService#startFlow ===> {}", workflowVO);
        ResultDTO resultDTO = new ResultDTO();
        resultDTO.setProcessInstanceId(workflowVO.getProcessInstanceId());
        ResultVO resultVO = new ResultVO();
        resultVO.setResultObj(resultDTO);
        return resultVO;
    }
    
    public void recordLog(WorkflowVO workflowVO) {
        log.info("WorkflowService#recordLog ===> {}", workflowVO);
    }
    
    public WorkflowVO cancel(WorkflowVO workflowVO) {
        log.info("WorkflowService#cancel ===> {}", workflowVO);
        return workflowVO;
    }
    
    public void rollback(String wfLogId) {
        log.info("WorkflowService#rollback ===> {}", wfLogId);
    }
}
