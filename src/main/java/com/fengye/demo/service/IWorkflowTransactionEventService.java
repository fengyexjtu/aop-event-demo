package com.fengye.demo.service;

import com.fengye.demo.vo.WorkflowVO;

/**
 * @author fengyexjtu@126.com
 * @since 2022-10-05
 */

public interface IWorkflowTransactionEventService {
    
    public WorkflowVO startProcess(WorkflowVO workflowVO);
}
