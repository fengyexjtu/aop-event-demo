package com.fengye.demo.service;

import com.fengye.demo.entity.Customer;

import java.util.Random;

public interface ICustomerService {
    public int save();

    public default Customer buildCustomer(){
        Customer customer = new Customer();
        customer.setAge(new Random().nextInt());
        customer.setName("test"+new Random().nextInt());
        return customer;
    }
}
