package com.fengye.demo.event;

import com.fengye.demo.vo.WorkflowVO;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * @author fengyexjtu@126.com
 * @since 2022-10-05
 */
@Getter
@Setter
public class WfStartEvent extends ApplicationEvent {
    public WfStartEvent(WorkflowVO workflowVO) {
        super(workflowVO);
        this.workflowVO = workflowVO;
    }
    
    private WorkflowVO workflowVO;
}
