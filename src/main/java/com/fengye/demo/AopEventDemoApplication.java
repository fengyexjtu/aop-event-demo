package com.fengye.demo;

import com.fengye.demo.service.IWorkflowTransactionEventService;
import com.fengye.demo.vo.WorkflowVO;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@Slf4j
//@EnableAspectJAutoProxy
@MapperScan("com.fengye.demo.mapper")
public class AopEventDemoApplication implements CommandLineRunner {
    @Autowired
    IWorkflowTransactionEventService workflowTransactionEventService;
    
    public static void main(String[] args) {
        // SpringApplication.run(AopEventDemoApplication.class, args);
        ConfigurableApplicationContext applicationContext = new SpringApplicationBuilder(AopEventDemoApplication.class)
                .web(WebApplicationType.NONE)
                .run(args);
    }
    
    @Override
    public void run(String... args) throws Exception {
        WorkflowVO workflowVO = new WorkflowVO();
        workflowVO.setProcessInstanceId("AopEventDemoApplication");
        workflowTransactionEventService.startProcess(workflowVO);
        log.info("CommandLineRunner...{}", workflowVO);
    }
}
