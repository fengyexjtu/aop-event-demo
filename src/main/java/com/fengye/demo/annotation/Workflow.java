package com.fengye.demo.annotation;

import com.fengye.demo.enums.OperationType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface Workflow {
    OperationType operation();
    
    boolean isRecordLog();
    
    String processKey() default "";
}
