package com.fengye.demo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.TransactionManager;

import javax.annotation.PostConstruct;

@Configuration
public class BeanConfiguration {

    @Autowired
    private ApplicationContext applicationContext;

    @PostConstruct
    public void setApplicationContext(){
        SpringContextUtil.setApplicationContext(applicationContext);
    }
}
