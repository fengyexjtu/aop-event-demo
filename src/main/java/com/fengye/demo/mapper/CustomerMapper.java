package com.fengye.demo.mapper;

import com.fengye.demo.entity.Customer;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CustomerMapper {
    public int insert(Customer customer);
}
