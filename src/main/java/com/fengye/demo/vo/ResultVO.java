package com.fengye.demo.vo;

import com.fengye.demo.dto.ResultDTO;
import lombok.Data;

/**
 * @author fengyexjtu@126.com
 * @since 2022-10-05
 */

@Data
public class ResultVO {
    private ResultDTO resultObj;
}
