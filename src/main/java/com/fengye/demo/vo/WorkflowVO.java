package com.fengye.demo.vo;

import com.fengye.demo.enums.OperationType;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fengyexjtu@126.com
 * @since 2022-10-05
 */
@Data
public class WorkflowVO {
    
    private String processInstanceId;
    
    private List<String> processInstanceIdList = new ArrayList<>();
    
    private OperationType operationType;
    
    private boolean recordLog;
    
    private String reason;
    
    private String wfLogId;
    
}
