package com.fengye.demo.enums;

import com.fengye.demo.event.WfCancelEvent;
import com.fengye.demo.event.WfCompleteEvent;
import com.fengye.demo.event.WfSignalEvent;
import com.fengye.demo.event.WfStartEvent;
import com.fengye.demo.event.WfTransferEvent;
import com.fengye.demo.vo.ResultVO;
import com.fengye.demo.vo.WorkflowVO;
import com.fengye.demo.service.WorkflowService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Collections;

@Slf4j
public enum OperationType {
    START{
        @Override
        @Transactional
        public void event(WorkflowService workflowService, ApplicationEventPublisher eventPublisher, WorkflowVO workflowVO) {
            ResultVO resultVO = workflowService.startFlow(workflowVO);
            log.info("workflowService.startFlow执行结束: {}", resultVO);
            String processInstanceId = resultVO.getResultObj().getProcessInstanceId();
            workflowVO.setProcessInstanceId(processInstanceId);
            workflowVO.setProcessInstanceIdList(Collections.singletonList(processInstanceId));
            eventPublisher.publishEvent(new WfStartEvent(workflowVO));
        }
    },
    COMPLETE{
        @Override
        public void event(WorkflowService workflowService, ApplicationEventPublisher eventPublisher, WorkflowVO workflowVO) {
            log.info("发布 complete 事件");
            eventPublisher.publishEvent(new WfCompleteEvent(workflowVO));
        }
    },
    CANCEL{
        @Override
        public void event(WorkflowService workflowService, ApplicationEventPublisher eventPublisher, WorkflowVO workflowVO) {
            log.info("发布 complete 事件");
            eventPublisher.publishEvent(new WfCancelEvent(workflowVO));
        }
    },
    TRANSFER{
        @Override
        public void event(WorkflowService workflowService, ApplicationEventPublisher eventPublisher, WorkflowVO workflowVO) {
            log.info("发布 complete 事件");
            eventPublisher.publishEvent(new WfTransferEvent(workflowVO));
        }
    },
    SIGNAL{
        @Override
        public void event(WorkflowService workflowService, ApplicationEventPublisher eventPublisher, WorkflowVO workflowVO) {
            log.info("发布 complete 事件");
            eventPublisher.publishEvent(new WfSignalEvent(workflowVO));
        }
    };
    
    public abstract void event(WorkflowService workflowService, ApplicationEventPublisher eventPublisher, WorkflowVO workflowVO);
}
