package com.fengye.demo.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Customer implements Serializable {
    private int id;
    private String name;
    private int age;
}
